create table datathon_2_db_full_lat_fix as
select 
 profitability_history + profitability_current profitability,
       case when profitability_history + profitability_current > 0 then 0 else 1 end as risk_flag,
       case when profitability_history + profitability_current > 2000 then 2000
            when profitability_history + profitability_current > 600 then 600
            when profitability_history + profitability_current > 201 then 200
            when profitability_history + profitability_current > 0 then 0
            else -1 end profitability_,
--t.bulstat_eik,
REGISTER,
FOUNDATION_DATE,
floor(MONTHS_BETWEEN (TO_DATE('01-01-2015','MM-DD-YYYY'),FOUNDATION_DATE )) FOUNDATION_DATE_DIFF,
case when FOUNDATION_DATE is not null then 1 else 0 end HAS_FOUNDATION_DATE,
cyr_to_lat(STATUS) STATUS,
case when cyr_to_lat(LEGAL_FORM)  = 'Opera' then 'Cluture'
when cyr_to_lat(LEGAL_FORM)  = 'Teatar' then 'Cluture'
when cyr_to_lat(LEGAL_FORM)  = 'Urid. lice kam nauchna org.' then 'Science'
when cyr_to_lat(LEGAL_FORM)  = 'Nauchna organizaciya' then 'Science'
when cyr_to_lat(LEGAL_FORM)  = 'CHitalishtno sdruzenie' then 'NGO'
when cyr_to_lat(LEGAL_FORM)  = 'Klon na CHUL s nestopanska cel' then 'NGO'
when cyr_to_lat(LEGAL_FORM)  = 'Fondaciya' then 'NGO'
when cyr_to_lat(LEGAL_FORM)  = 'DZZD' then 'NGO'
when cyr_to_lat(LEGAL_FORM)  = 'Mestno podelenie (UL)' then 'Militar'
when cyr_to_lat(LEGAL_FORM)  = 'Raionna kolegiya' then 'Militar'
when cyr_to_lat(LEGAL_FORM)  = 'podelenie' then 'Healt'
when cyr_to_lat(LEGAL_FORM)  = 'Nac. centar.za opazv. na obsht. zdrave' then 'Healt'
when cyr_to_lat(LEGAL_FORM)  = 'Lechebno zavedenie za bolnichna pomosht' then 'Healt'
when cyr_to_lat(LEGAL_FORM)  = 'Drugo lechebno zavedenie' then 'Healt'
when cyr_to_lat(LEGAL_FORM)  = 'Zanayatchiisko predpriyatie' then 'ET'
when cyr_to_lat(LEGAL_FORM)  = 'KDA' then 'Corporate'
when cyr_to_lat(LEGAL_FORM)  = 'Targovec' then 'ET'
when cyr_to_lat(LEGAL_FORM)  = 'Bolnichno zavedenie' then 'Corporate'
when cyr_to_lat(LEGAL_FORM)  = 'KD' then 'Corporate'
when cyr_to_lat(LEGAL_FORM)  = 'OP' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'klon' then 'Corporate'
when cyr_to_lat(LEGAL_FORM)  = 'Advokatsko druzestvo' then 'Corporate'
when cyr_to_lat(LEGAL_FORM)  = 'Sdruzenie' then 'Corporate'
when cyr_to_lat(LEGAL_FORM)  = 'EAD' then 'Corporate'
when cyr_to_lat(LEGAL_FORM)  = 'SD' then 'Corporate'
when cyr_to_lat(LEGAL_FORM)  = 'Kooperaciya' then 'Corporate'
when cyr_to_lat(LEGAL_FORM)  = 'AD' then 'Corporate'
when cyr_to_lat(LEGAL_FORM)  = 'ET' then 'ET'
when cyr_to_lat(LEGAL_FORM)  = 'OOD' then 'Corporate'
when cyr_to_lat(LEGAL_FORM)  = 'EOOD' then 'Corporate'
when cyr_to_lat(LEGAL_FORM)  = 'centralna banka' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Notarialna kamara' then 'Loyer'
when cyr_to_lat(LEGAL_FORM)  = 'Advokatska kolegiya' then 'Loyer'
when cyr_to_lat(LEGAL_FORM)  = 'Advokatsko sadruzie' then 'Loyer'
when cyr_to_lat(LEGAL_FORM)  = 'Narodno sabranie' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Administraciya na prezidenta na RB' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Ministerski savet' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Prokuratura' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Konsulsko predstavitelstvo' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Ur. lice kam kulturna instituciya' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Agenciya' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'ED' then 'ET'
when cyr_to_lat(LEGAL_FORM)  = 'Organizaciya v sastava na VU' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Religiozna instituciya (BPC)' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Nacionalna zdravno-osiguritelna kasa' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Sdruzenie za napoyavane' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Zaved.za opazv.zdr. na maika i dete' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Predstavitelstvo na chuzdestr. AP' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Biblioteka' then 'Science'
when cyr_to_lat(LEGAL_FORM)  = 'Kolez izvan sastava na VU' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Kulturen centar na druga darzava' then 'Science'
when cyr_to_lat(LEGAL_FORM)  = 'Sdruzenie na sobstvenicite' then 'Corporate'
when cyr_to_lat(LEGAL_FORM)  = 'Predstavitelstvo na mezd. organiz.' then 'Healt'
when cyr_to_lat(LEGAL_FORM)  = 'Nefizichesko lice - osiguritel' then 'ET'
when cyr_to_lat(LEGAL_FORM)  = 'Predstavit. na drugo chuzdestr.lice' then 'ET'
when cyr_to_lat(LEGAL_FORM)  = 'Nepravit.org.,sazd.s narochno osnov.' then 'ET'
when cyr_to_lat(LEGAL_FORM)  = 'Drug vid subekt' then 'ET'
when cyr_to_lat(LEGAL_FORM)  = 'CHuzd. NFL s myasto na stop. deinost' then 'ET'
when cyr_to_lat(LEGAL_FORM)  = 'ADSIC' then 'ET'
when cyr_to_lat(LEGAL_FORM)  = 'Instituciya, sazd. sas spec. zakon' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'DP' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Visshe uchilishte' then 'Science'
when cyr_to_lat(LEGAL_FORM)  = 'TP' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'KCHT' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'UL v sferata na darz. vlast' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = '' then ''
when cyr_to_lat(LEGAL_FORM)  = 'Darzavna komisiya' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Darzavna agenciya' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Ministerstvo' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Izpalnitelna agenciya' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Oblastna administraciya' then 'Government'
when cyr_to_lat(LEGAL_FORM)  = 'Diplomatichesko predstavitelstvo' then 'Government'
else cyr_to_lat(LEGAL_FORM)  end  LEGAL_FORM,
case when AVARAGE_ANNUAL_STAFF_2013 in ('45992',
'45962',
'45931',
'45901',
'45870',
'45839',
'45809',
'45778',
'45748',
'45717',
'45689',
'45658',
'42992',
'42986',
'42982',
'42981',
'42980',
'42979',
'42976',
'42960',
'42957',
'42954',
'42953',
'42952',
'42951',
'42950',
'42949',
'42948',
'42931',
'42929',
'42926',
'42921',
'42920',
'42919',
'42918',
'42917',
'42907',
'42904',
'42897',
'42895',
'42894',
'42893',
'42892',
'42891',
'42890',
'42889',
'42888',
'42887',
'42886',
'42885',
'42884',
'42883',
'42882',
'42881',
'42880',
'42879',
'42878',
'42877',
'42876',
'42875',
'42874',
'42873',
'42872',
'42871',
'42870',
'42869',
'42868',
'42867',
'42866',
'42865',
'42864',
'42863',
'42862',
'42861',
'42860',
'42859',
'42858',
'42857',
'42856',
'42839',
'42838',
'42835',
'42833',
'42832',
'42831',
'42830',
'42829',
'42828',
'42827',
'42826',
'42803',
'42800',
'42798',
'42797',
'42795',
'42789',
'42779',
'42777',
'42774',
'42773',
'42772',
'42771',
'42770',
'42769',
'42768',
'42767',
'42748',
'42744',
'42741',
'42740',
'42738',
'42737',
'42736',
'27729',
'27699',
'27668',
'27638',
'27607',
'27576',
'27546',
'27515',
'27485',
'27454',
'27426',
'27395') then -1 else AVARAGE_ANNUAL_STAFF_2013 end AVARAGE_ANNUAL_STAFF_2013,
case when AVARAGE_ANNUAL_STAFF_2014 in ('27395',
'27426',
'27454',
'27485',
'27515',
'27546',
'27576',
'27607',
'27638',
'27668',
'27699',
'27729',
'42736',
'42737',
'42738',
'42739',
'42740',
'42760',
'42767',
'42768',
'42769',
'42770',
'42771',
'42772',
'42776',
'42777',
'42779',
'42780',
'42795',
'42796',
'42798',
'42799',
'42800',
'42825',
'42826',
'42827',
'42828',
'42829',
'42830',
'42833',
'42835',
'42836',
'42843',
'42846',
'42853',
'42856',
'42857',
'42858',
'42859',
'42860',
'42861',
'42862',
'42863',
'42864',
'42865',
'42866',
'42867',
'42868',
'42869',
'42870',
'42871',
'42872',
'42873',
'42874',
'42875',
'42876',
'42877',
'42878',
'42879',
'42880',
'42881',
'42882',
'42883',
'42884',
'42885',
'42886',
'42887',
'42888',
'42889',
'42890',
'42891',
'42892',
'42893',
'42894',
'42895',
'42900',
'42903',
'42904',
'42917',
'42920',
'42921',
'42925',
'42941',
'42948',
'42949',
'42950',
'42951',
'42952',
'42953',
'42954',
'42955',
'42958',
'42963',
'42979',
'42980',
'42981',
'42982',
'42985',
'42987',
'42989',
'42990',
'43001',
'45658',
'45689',
'45717',
'45748',
'45778',
'45809',
'45839',
'45870',
'45901',
'45931',
'45962',
'45992') then -1 else AVARAGE_ANNUAL_STAFF_2014 end AVARAGE_ANNUAL_STAFF_2014,
HEALTH_INSURED_NUMBER,
SOCIAL_INSURED_NUMBER,
NET_INCOME_FROM_SALES_2013,
NET_INCOME_FROM_SALES_2012,
TOTAL_REVENUE_2013,
TOTAL_REVENUE_2012,
OWN_CAPITAL_2012,
OWN_CAPITAL_2013,
PROFIT_LOSS_2012,
PROFIT_LOSS_2013,
cyr_to_lat (COMMERCIAL_REG_DISTRICT) COMMERCIAL_REG_DISTRICT,
cyr_to_lat (COMMERCIAL_REG_MUNICIPALITY) COMMERCIAL_REG_MUNICIPALITY,
substr(ZIP,1,3) ZIP,
cyr_to_lat (MAIN_OFFICE) MAIN_OFFICE,
case when cyr_to_lat (VAT_STATUS) = 'Deregistriran' then 0
  when cyr_to_lat (VAT_STATUS) = 'V registraciya' then 1
    else null end  VAT_STATUS,
VAT_DATE,
cyr_to_lat (VAT_ADDRESS_DISTRICT) VAT_ADDRESS_DISTRICT,
cyr_to_lat (VAT_ADDRESS_MUNICIPALITY) VAT_ADDRESS_MUNICIPALITY,
cyr_to_lat (VAT_ADDRESS_MAIN_OFFICE) VAT_ADDRESS_MAIN_OFFICE,
cyr_to_lat (NII_ADDRESS_MAIN_OFFICE) NII_ADDRESS_MAIN_OFFICE,
COMPANYKEY 
from datathon_2_db_full_fix t
where cyr_to_lat(t.status) not in ('Prekratyavane na proizvodstvoto po likvidaciya',
'Obyaven v nesastoyatelnost',
'Preustroistvo na kooperaciya, forma: otdelyane',
'Vazobnovyavane na likvidaciyata',
'Preobrazuvan, forma: prehvarlyane na imushtestvo',
'Prekrateno proizvodstvoto po nesastoyatelnost',
'Preobrazuvan, forma: slivane')
--where t.profitability_history is null or t.profitability_current is null
and cyr_to_lat(t.legal_form) <> 'centralna banka'
--where 
